def measure (reps = 1, &prc)
  results = []
  reps.times do
    start = Time.now
    prc.call
    fin = Time.now
    results.push(fin - start)
  end
  results.inject{ |sum, el| sum + el }.to_f / results.size
end
