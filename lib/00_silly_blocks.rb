def reverser(&prc)
  text = prc.call
  text.split(" ").map(&:reverse).join(" ")
end

def adder(value = 1, &prc)
  prc.call + value
end

def repeater(reps = 1, &prc)
  reps.times { prc.call }
end
